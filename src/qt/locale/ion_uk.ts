<TS language="uk" version="2.1">
<context>
    <name>AddressBookPage</name>
    <message>
        <location filename="../forms/addressbookpage.ui" line="+67"/>
        <source>Right-click to edit address or label</source>
        <translation>Натисніть правою кнопкою миші, щоб редагувати адресу або мітку</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Create a new address</source>
        <translation>Створити нову адресу</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;New</source>
        <translation>Новий</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Copy the currently selected address to the system clipboard</source>
        <translation>Скопіювати вибрану адресу в буфер обміну</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy</source>
        <translation>Копіювати</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Delete the currently selected address from the list</source>
        <translation>Видалити вибрану адресу зі списку</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Delete</source>
        <translation>Видалити</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Експортуйтувати дані поточної вкладки у файл</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Export</source>
        <translation>Експорт</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>C&amp;lose</source>
        <translation>Закрити</translation>
    </message>
    <message>
        <location filename="../addressbookpage.cpp" line="+44"/>
        <source>Choose the address to send coins to</source>
        <translation>Виберіть адресу надсилання монет</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Choose the address to receive coins with</source>
        <translation>Виберіть адресу отримання монет</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;hoose</source>
        <translation>Вибір</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Sending addresses</source>
        <translation>Адреса відправки</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Receiving addresses</source>
        <translation>Адреса отримання</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>These are your ION addresses for sending payments. Always check the amount and the receiving address before sending coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>These are your ION addresses for receiving payments. It is recommended to use a new receiving address for each transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Copy Address</source>
        <translation>Копіювати адресу</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy &amp;Label</source>
        <translation>Копіювати мітку</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Edit</source>
        <translation>Редагувати</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Export Address List</source>
        <translation>Ексортувати список адрес</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Файл, розділений комами (*.csv)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Exporting Failed</source>
        <translation>Не вдалося експортувати</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error trying to save the address list to %1. Please try again.</source>
        <translation>Виникла помилка при спробі зберегти список адрес у %1. Будь ласка, спробуйте ще раз.</translation>
    </message>
</context>
<context>
    <name>AddressTableModel</name>
    <message>
        <location filename="../addresstablemodel.cpp" line="+199"/>
        <source>Label</source>
        <translation>Мітка</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>(no label)</source>
        <translation>(без міток)</translation>
    </message>
</context>
<context>
    <name>AskPassphraseDialog</name>
    <message>
        <location filename="../forms/askpassphrasedialog.ui" line="+26"/>
        <source>Passphrase Dialog</source>
        <translation>Кодове слово</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter passphrase</source>
        <translation>Введіть кодове слово</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>New passphrase</source>
        <translation>Нове кодове слово</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Repeat new passphrase</source>
        <translation>Повторіть кодове слово</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Serves to disable the trivial sendmoney when OS account compromised. Provides no real security.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>For anonymization, automint, and staking only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../askpassphrasedialog.cpp" line="+48"/>
        <source>Enter the new passphrase to the wallet.&lt;br/&gt;Please use a passphrase of &lt;b&gt;ten or more random characters&lt;/b&gt;, or &lt;b&gt;eight or more words&lt;/b&gt;.</source>
        <translation>Введіть нове кодове слово гамамнця. &lt;br/&gt;Будь ласка, використовуйте кодове слово з &lt;b&gt;десяти і більше хаотичних символів&lt;/b&gt;, або &lt;b&gt;восьми і більше слів &lt;/b&gt;.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Encrypt wallet</source>
        <translation>Шифрувати гаманець</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>This operation needs your wallet passphrase to unlock the wallet.</source>
        <translation>Для розблокування гаманця потрібно його кодове слово.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unlock wallet</source>
        <translation>Розблокувати гаманець</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This operation needs your wallet passphrase to decrypt the wallet.</source>
        <translation>Для розшифрування гаманція потрібно його кодове слово.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Decrypt wallet</source>
        <translation>Розшифрувати гаманець</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Change passphrase</source>
        <translation>Змінити кодове слово</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the old and new passphrase to the wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>Confirm wallet encryption</source>
        <translation>Підтвердження шифрування гаманця</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>ION will close now to finish the encryption process. Remember that encrypting your wallet cannot fully protect your IONs from being stolen by malware infecting your computer.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-8"/>
        <source>Are you sure you wish to encrypt your wallet?</source>
        <translation>Ви впевнені, що хочете зашифрувати Ваш гаманець?</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Warning: If you encrypt your wallet and lose your passphrase, you will &lt;b&gt;LOSE ALL OF YOUR ION&lt;/b&gt;!</source>
        <translation>Попередження: Якщо після шифрування гаманця Ви &lt;b&gt;загубите кодове слово, то ви втратите всі Ваші ION&apos;и&lt;/b&gt;!</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+45"/>
        <source>Wallet encrypted</source>
        <translation>Гаманець зашифровано</translation>
    </message>
    <message>
        <location line="-39"/>
        <source>IMPORTANT: Any previous backups you have made of your wallet file should be replaced with the newly generated, encrypted wallet file. For security reasons, previous backups of the unencrypted wallet file will become useless as soon as you start using the new, encrypted wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+5"/>
        <location line="+31"/>
        <location line="+4"/>
        <source>Wallet encryption failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-39"/>
        <source>Wallet encryption failed due to an internal error. Your wallet was not encrypted.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <location line="+35"/>
        <source>The supplied passphrases do not match.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-26"/>
        <source>Wallet unlock failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+8"/>
        <location line="+13"/>
        <source>The passphrase entered for the wallet decryption was incorrect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-14"/>
        <source>Wallet decryption failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Wallet passphrase was successfully changed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <location line="+24"/>
        <source>Warning: The Caps Lock key is on!</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BanTableModel</name>
    <message>
        <location filename="../bantablemodel.cpp" line="+87"/>
        <source>IP/Netmask</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Banned Until</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Bip38ToolDialog</name>
    <message>
        <location filename="../forms/bip38tooldialog.ui" line="+14"/>
        <source>BIP 38 Tool</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;BIP 38 Encrypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <location line="+400"/>
        <source>Address:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-424"/>
        <source>Enter a ION Address that you would like to encrypt using BIP 38. Enter a passphrase in the middle box. Press encrypt to compute the encrypted private key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>The ION address to encrypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+220"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-210"/>
        <location line="+220"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-200"/>
        <location line="+220"/>
        <source>Passphrase: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-193"/>
        <location line="+149"/>
        <source>Encrypted Key:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-127"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Encrypt the private key for this ION address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <location line="+171"/>
        <source>Reset all fields</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-72"/>
        <source>The encrypted private key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+55"/>
        <source>Decrypt the entered key using the passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-168"/>
        <source>Encrypt &amp;Key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <location line="+171"/>
        <source>Clear &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-112"/>
        <source>&amp;BIP 38 Decrypt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the BIP 38 encrypted private key. Enter the passphrase in the middle box. Click Decrypt Key to compute the private key. After the key is decrypted, clicking &apos;Import Address&apos; will add this private key to the wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>Decrypt &amp;Key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+77"/>
        <source>Decrypted Key:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Import Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../bip38tooldialog.cpp" line="+29"/>
        <source>Click &quot;Decrypt Key&quot; to compute key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>The entered passphrase is invalid. </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Allowed: 0-9,a-z,A-Z,</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>The entered address is invalid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+8"/>
        <source>Please check the address and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The entered address does not refer to a key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+63"/>
        <source>Wallet unlock was cancelled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-56"/>
        <source>Private key for the entered address is not available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <source>Failed to decrypt.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Please check the key and passphrase and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Data Not Valid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Please try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Please wait while key is imported</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Key Already Held By Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Error Adding Key To Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Successfully Added Private Key To Wallet</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BitcoinGUI</name>
    <message>
        <location filename="../bitcoingui.cpp" line="+123"/>
        <source>Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Node</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+174"/>
        <source>&amp;Overview</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show general overview of wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Send</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Receive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse transaction history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Privacy Actions for xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>&amp;Governance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Proposals</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Quit application</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>About &amp;Qt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about Qt</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Options...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Show / Hide</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show or hide the main Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Encrypt Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Encrypt the private keys that belong to your wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Backup Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Backup wallet to another location</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Change Passphrase...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Change the passphrase used for wallet encryption</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Unlock Wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unlock wallet</source>
        <translation>Розблокувати гаманець</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Lock Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign &amp;message...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Verify message...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show diagnostic information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Debug console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open debugging console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Network Monitor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show network monitor</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Peers list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show peers info</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet &amp;Repair</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show wallet repair options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Open configuration file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Automatic &amp;Backups</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show automatically created wallet backups</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Sending addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used sending addresses and labels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Receiving addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show the list of used receiving addresses and labels</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Multisignature creation...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Create a new multisignature address and add it to this wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Multisignature spending...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Spend from a multisignature address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Multisignature signing...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sign with a multisignature address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;URI...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+490"/>
        <source>Processed %n blocks of transaction history.</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Synchronizing additional data: %p%</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>%1 behind. Scanning block %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+242"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt; for anonymization and staking only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Tor is &lt;b&gt;enabled&lt;/b&gt;: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-779"/>
        <source>&amp;File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Help</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Tabs toolbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-448"/>
        <location line="+938"/>
        <source>ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-742"/>
        <source>Send coins to a ION address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Request payments (generates QR codes and ion: URIs)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Privacy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <source>&amp;About ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show information about ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Modify configuration options for ION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Sign messages with your ION addresses to prove you own them</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Verify messages to ensure they were signed with specified ION addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;BIP38 tool</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Encrypt and decrypt private keys using a passphrase</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;MultiSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>MultiSend Settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Open Wallet &amp;Configuration File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Open &amp;Masternode Configuration File</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Masternode configuration file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Open a ION: URI or payment request</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Blockchain explorer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Block explorer window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Show the ION Core help message to get a list with possible ION command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+234"/>
        <source>ION Core client</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+219"/>
        <source>%n active connection(s) to ION network</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Synchronizing with network...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Importing blocks from disk...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Reindexing blocks on disk...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>No block source available...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Up to date</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+41"/>
        <source>%n hour(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>%n day(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <location line="+4"/>
        <source>%n week(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+0"/>
        <source>%1 and %2</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+0"/>
        <source>%n year(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Catching up...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Last received block was generated %1 ago.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions after this will not yet be visible.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+69"/>
        <source>Sent transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Incoming transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Sent MultiSend transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Date: %1
Amount: %2
Type: %3
Address: %4
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>Staking is active
 MultiSend: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+6"/>
        <source>Active</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-6"/>
        <location line="+6"/>
        <source>Not Active</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Staking is not active
 MultiSend: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>AutoMint is currently enabled and set to </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>AutoMint is disabled</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;unlocked&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Wallet is &lt;b&gt;encrypted&lt;/b&gt; and currently &lt;b&gt;locked&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ion.cpp" line="+478"/>
        <source>A fatal error occurred. ION can no longer continue safely and will quit.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>BlockExplorer</name>
    <message>
        <location filename="../forms/blockexplorer.ui" line="+14"/>
        <source>Blockchain Explorer</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Back</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Forward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Address / Block / Transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../blockexplorer.cpp" line="+480"/>
        <source>Not all transactions will be shown. To view all transactions you need to set txindex=1 in the configuration file (ioncoin.conf).</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ClientModel</name>
    <message>
        <location filename="../clientmodel.cpp" line="+81"/>
        <source>Total: %1 (IPv4: %2 / IPv6: %3 / Tor: %4 / Unknown: %5)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+101"/>
        <source>Network Alert</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>CoinControlDialog</name>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+48"/>
        <source>Quantity:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Bytes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+45"/>
        <source>Amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Priority:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+45"/>
        <source>Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-182"/>
        <source>Coin Selection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+214"/>
        <source>Dust:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>After Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>(un)select all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>toggle lock state</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Tree mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>List mode</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>(1 locked)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Received with address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Confirmations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Priority</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+60"/>
        <source>Copy address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+26"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-25"/>
        <source>Copy transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Lock unspent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unlock unspent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Copy quantity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+147"/>
        <source>Please switch to &quot;List mode&quot; to use this function.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+216"/>
        <source>highest</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>higher</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>high</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>medium-high</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="-334"/>
        <location filename="../coincontroldialog.cpp" line="+2"/>
        <source>medium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+2"/>
        <source>low-medium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>low</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>lower</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>lowest</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>(%1 locked)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+62"/>
        <source>none</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+126"/>
        <source>yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../forms/coincontroldialog.ui" line="+80"/>
        <location filename="../coincontroldialog.cpp" line="+0"/>
        <source>no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../coincontroldialog.cpp" line="+15"/>
        <source>This label turns red, if the transaction size is greater than 1000 bytes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+5"/>
        <source>This means a fee of at least %1 per kB is required.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-4"/>
        <source>Can vary +/- 1 byte per input.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transactions with higher priority are more likely to get included into a block.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This label turns red, if the priority is smaller than &quot;medium&quot;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>This label turns red, if any recipient receives an amount smaller than %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Can vary +/- %1 uion per input.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <location line="+81"/>
        <source>(no label)</source>
        <translation>(без міток)</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>change from %1 (%2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>(change)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>EditAddressDialog</name>
    <message>
        <location filename="../forms/editaddressdialog.ui" line="+14"/>
        <source>Edit Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>&amp;Label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>The label associated with this address list entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>The address associated with this address list entry. This can only be modified for sending addresses.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../editaddressdialog.cpp" line="+28"/>
        <source>New receiving address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>New sending address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit receiving address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Edit sending address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+67"/>
        <source>The entered address &quot;%1&quot; is not a valid ION address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>The entered address &quot;%1&quot; is already in the address book.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Could not unlock wallet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>New key generation failed.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>FreespaceChecker</name>
    <message>
        <location filename="../intro.cpp" line="+70"/>
        <source>A new data directory will be created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Directory already exists. Add %1 if you intend to create a new directory here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Path already exists, and is not a directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Cannot create data directory here.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>GovernancePage</name>
    <message>
        <location filename="../forms/governancepage.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>GOVERNANCE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+141"/>
        <source>Update Proposals</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Next super block:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <location line="+14"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-63"/>
        <source>Blocks to next super block:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Days to budget payout (estimate):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Allotted budget:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Budget left:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Masternodes count:</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>HelpMessageDialog</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+37"/>
        <source>version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <location line="+2"/>
        <source>(%1-bit)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>About ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Usage:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>command-line options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>UI Options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose data directory on startup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Show splash screen on startup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-3"/>
        <source>Set language, for example &quot;de_DE&quot; (default: system locale)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Start minimized</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set SSL root certificates for payment request (default: -system-)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>Intro</name>
    <message>
        <location filename="../forms/intro.ui" line="+14"/>
        <source>Welcome</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Welcome to ION Core.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>As this is the first time the program is launched, you can choose where ION Core will store its data.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>ION Core will download and store a copy of the ION block chain. At least %1GB of data will be stored in this directory, and it will grow over time. The wallet will also be stored in this directory.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Use the default data directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Use a custom data directory:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../intro.cpp" line="+77"/>
        <source>ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Specified data directory &quot;%1&quot; cannot be created.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>%1 GB of free space available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>(of %1 GB needed)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MasternodeList</name>
    <message>
        <location filename="../forms/masternodelist.ui" line="+14"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>MASTERNODES</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+83"/>
        <source>Note: Status of your masternodes in local wallet can potentially be slightly incorrect.&lt;br /&gt;Always wait for wallet to sync additional data and then double check from another node&lt;br /&gt;if your node should be running but you still see &quot;MISSING&quot; in &quot;Status&quot; field.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>Alias</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Protocol</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Active</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Last Seen (UTC)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Pubkey</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>S&amp;tart alias</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Start &amp;all</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Start &amp;MISSING</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Update status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Status will be updated automatically in (sec):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../masternodelist.cpp" line="+52"/>
        <source>Start alias</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+189"/>
        <source>Confirm masternode start</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to start masternode %1?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Confirm all masternodes start</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to start ALL masternodes?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Command is not available right now</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>You can&apos;t use this command until masternode list is synced</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Confirm missing masternodes start</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to start MISSING masternodes?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MultiSendDialog</name>
    <message>
        <location filename="../forms/multisenddialog.ui" line="+17"/>
        <source>MultiSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+87"/>
        <source>Enter whole numbers 1 - 100</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enter % to Give (1-100)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Enter Address to Send to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-106"/>
        <source>MultiSend allows you to automatically send up to 100% of your stake or masternode reward to a list of other ION addresses after it matures.
To Add: enter percentage to give and ION address to add to the MultiSend vector.
To Delete: Enter address to delete and press delete.
MultiSend will not be activated unless you have clicked Activate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+207"/>
        <source>Add to MultiSend Vector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>Deactivate MultiSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Deactivate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-128"/>
        <source>Choose an address from the address book</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-66"/>
        <source>Percentage of stake to send</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Percentage:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Address to send portion of stake to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Address:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Enter a label for this address to add it to your address book</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+65"/>
        <source>Delete Address From MultiSend Vector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Activate MultiSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Activate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-47"/>
        <source>View MultiSend Vector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>View MultiSend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-154"/>
        <source>Send For Stakes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Send For Masternode Rewards</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../multisenddialog.cpp" line="+64"/>
        <source>(no label)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>MultiSend Active for Stakes and Masternode Rewards</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>MultiSend Active for Stakes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>MultiSend Active for Masternode Rewards</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>MultiSend Not Active</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>The entered address: %1 is invalid.
Please check the address and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>The total amount of your MultiSend vector is over 100% of your stake reward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Saved the MultiSend to memory, but failed saving properties to the database.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>MultiSend Vector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Removed %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not locate address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Unable to activate MultiSend, check MultiSend vector</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Need to select to send on stake and/or masternode rewards</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>MultiSend activated but writing settings to DB failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>MultiSend activated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>First Address Not Valid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>MultiSend deactivated but writing settings to DB failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>MultiSend deactivated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-97"/>
        <source>Please Enter 1 - 100 for percent.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>MultisigDialog</name>
    <message>
        <location filename="../forms/multisigdialog.ui" line="+20"/>
        <source>Multisignature Address Interactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Create MultiSignature &amp;Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>How many people must sign to verify a transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Enter the minimum number of signatures required to sign transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Address Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Add another address that could sign to verify a transaction from the multisig address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Add Address / Key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Local addresses or public keys that can sign:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>Create a new multisig address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>C&amp;reate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <location line="+345"/>
        <location line="+179"/>
        <source>Status:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-487"/>
        <source>Use below to quickly import an address by its redeem. Don't forget to add a label before clicking import!
Keep in mind, the wallet will rescan the blockchain to find transactions containing the new address.
Please be patient after clicking import.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>&amp;Import Redeem</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>&amp;Create MultiSignature Tx</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Inputs:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Coin Control</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Quantity Selected:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+14"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>Amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Add an input to fund the outputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add a Raw Input</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>Address / Amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Add destinations to send ION to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Destination</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+81"/>
        <source>Create a transaction object using the given inputs to the given outputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Cr&amp;eate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+76"/>
        <source>&amp;Sign MultiSignature Tx</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Transaction Hex:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>Sign the transaction from this wallet or from provided private keys</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;ign</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;DISABLED until transaction has been signed enough times.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Co&amp;mmit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Add private keys to sign the transaction with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add Private &amp;Key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Sign with only private keys (Not Recommened)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../multisigdialog.cpp" line="+299"/>
        <source>Invalid Tx Hash.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Vout position must be positive.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+535"/>
        <source>Maximum possible addresses reached. (15)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+110"/>
        <source>Vout Position: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+57"/>
        <source>Amount: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Maximum (15)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OpenURIDialog</name>
    <message>
        <location filename="../forms/openuridialog.ui" line="+14"/>
        <source>Open URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Open payment request from URI or file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>URI:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Select payment request file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../openuridialog.cpp" line="+45"/>
        <source>Select payment request file to open</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OptionsDialog</name>
    <message>
        <location filename="../forms/optionsdialog.ui" line="+14"/>
        <source>Options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Main</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Size of &amp;database cache</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Number of script &amp;verification threads</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>(0 = auto, &lt;0 = leave that many cores free)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+141"/>
        <source>W&amp;allet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <source>If you disable the spending of unconfirmed change, the change from a transaction&lt;br/&gt;cannot be used until that transaction has at least one confirmation.&lt;br/&gt;This also affects how your balance is computed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <source>Automatically open the ION client port on the router. This only works when your router supports UPnP and it is enabled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Accept connections from outside</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow incoming connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Connect through SOCKS5 proxy (default proxy):</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-82"/>
        <source>Expert</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-236"/>
        <source>Automatically start ION after logging in to the system.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Start ION on system login</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+239"/>
        <source>Whether to show coin control features or not.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable coin &amp;control features</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Show additional tab listing all your masternodes in first sub-tab&lt;br/&gt;and all masternodes on the network in second sub-tab.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Masternodes Tab</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Spend unconfirmed change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>&amp;Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+192"/>
        <source>The user interface language can be set here. This setting will take effect after restarting ION.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Language missing or translation incomplete? Help contributing translations here:
https://www.transifex.com/ioncoincore/ioncore</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-204"/>
        <source>Map port using &amp;UPnP</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-187"/>
        <source>Enable automatic minting of ION units to xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enable xION Automint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Enable automatic xION minting from specific addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enable Automint Addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Percentage of incoming ION which get automatically converted to xION via Zerocoin Protocol (min: 10%)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Percentage of autominted xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <location line="+16"/>
        <source>Wait with automatic conversion to Zerocoin until enough ION for this denomination is available</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-13"/>
        <source>Preferred Automint xION Denomination</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Stake split threshold:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+98"/>
        <source>Connect to the ION network through a SOCKS5 proxy.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Proxy &amp;IP:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>IP address of the proxy (e.g. IPv4: 127.0.0.1 / IPv6: ::1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Port:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Port of the proxy (e.g. 9050)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+36"/>
        <source>&amp;Window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Show only a tray icon after minimizing the window.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize to the tray instead of the taskbar</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Minimize instead of exit the application when the window is closed. When this option is enabled, the application will be closed only after selecting Quit in the menu.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>M&amp;inimize on close</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>&amp;Display</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>User Interface &amp;language:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <source>User Interface Theme:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Unit to show amounts in:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Choose the default subdivision unit to show in the interface and when sending coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Decimal digits</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <location line="+6"/>
        <source>Hide empty balances</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Hide orphan stakes in transaction lists</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Hide orphan stakes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <location line="+10"/>
        <source>Third party URLs (e.g. a block explorer) that appear in the transactions tab as context menu items. %s in the URL is replaced by transaction hash. Multiple URLs are separated by vertical bar |.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>Third party transaction URLs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>Active command-line options that override above options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+43"/>
        <source>Reset all client options to default.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Reset Options</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+61"/>
        <source>&amp;OK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../optionsdialog.cpp" line="+92"/>
        <source>Any</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>default</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>none</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+98"/>
        <source>Confirm options reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+29"/>
        <source>Client restart required to activate changes.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-29"/>
        <source>Client will be shutdown, do you want to proceed?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>This change would require a client restart.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>The supplied proxy address is invalid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>The supplied proxy port is invalid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>The supplied proxy settings are invalid.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>OverviewPage</name>
    <message>
        <location filename="../forms/overviewpage.ui" line="+20"/>
        <source>Form</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+270"/>
        <location line="+192"/>
        <location line="+403"/>
        <source>Available:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-579"/>
        <location line="+192"/>
        <source>Your current spendable balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-160"/>
        <source>Total Balance, including all unavailable coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>ION Balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+130"/>
        <source>Pending:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Total of transactions that have yet to be confirmed, and do not yet count toward the spendable balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <location line="+351"/>
        <source>Immature:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-335"/>
        <source>Staked or masternode rewards that has not yet matured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+85"/>
        <source>Current locked balance in watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <location line="+19"/>
        <source>Your current ION balance, unconfirmed and immature transactions included</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+96"/>
        <source>xION Balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <location line="+17"/>
        <source>Mature: more than 20 confirmation and more than 1 mint of the same denomination after it was minted.
These xION are spendable.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <location line="+17"/>
        <location line="+14"/>
        <location line="+17"/>
        <source>Unconfirmed: less than 20 confirmations
Immature: confirmed, but less than 1 mint of the same denomination after it was minted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-725"/>
        <location line="+852"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the ION network after a connection is established, but this process has not completed yet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-990"/>
        <source>OVERVIEW</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+125"/>
        <source>Combined Balance (including unconfirmed and immature coins)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Combined Balance</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+355"/>
        <source>Unconfirmed transactions to watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+57"/>
        <source>Staked or masternode rewards in watch-only addresses that has not yet matured</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-315"/>
        <location line="+394"/>
        <location line="+261"/>
        <source>Total:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-220"/>
        <source>Current total balance in watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-285"/>
        <source>Watch-only:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+51"/>
        <source>Your current balance in watch-only addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-61"/>
        <source>Spendable:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+191"/>
        <location line="+19"/>
        <source>Locked ION or Masternode collaterals. These are excluded from xION minting.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-16"/>
        <source>Locked:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+260"/>
        <source>Unconfirmed:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+58"/>
        <location line="+19"/>
        <source>Your current xION balance, unconfirmed and immature xION included.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+84"/>
        <source>Recent transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../overviewpage.cpp" line="+131"/>
        <location line="+1"/>
        <source>out of sync</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+111"/>
        <source>Current percentage of xION.
If AutoMint is enabled this percentage will settle around the configured AutoMint percentage (default = 10%).
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>AutoMint is currently enabled and set to </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>To disable AutoMint add &apos;enablezeromint=0&apos; in ioncoin.conf.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>AutoMint is currently disabled.
To enable AutoMint change &apos;enablezeromint=0&apos; to &apos;enablezeromint=1&apos; in ioncoin.conf</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PaymentServer</name>
    <message>
        <location filename="../paymentserver.cpp" line="+291"/>
        <location line="+205"/>
        <location line="+33"/>
        <location line="+104"/>
        <location line="+12"/>
        <location line="+14"/>
        <source>Payment request error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-275"/>
        <location line="+12"/>
        <location line="+5"/>
        <source>URI handling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-16"/>
        <source>Payment request fetch URL is invalid: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Payment request file handling</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-18"/>
        <source>Invalid payment address %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-104"/>
        <source>Cannot start ion: click-to-pay handler</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+110"/>
        <source>URI cannot be parsed! This can be caused by an invalid ION address or malformed URI parameters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Payment request file cannot be read! This can be caused by an invalid payment request file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+67"/>
        <location line="+8"/>
        <location line="+30"/>
        <source>Payment request rejected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-38"/>
        <source>Payment request network doesn&apos;t match client network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Payment request has expired.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment request is not initialized.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Unverified payment requests to custom payment scripts are unsupported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Requested payment amount of %1 is too small (considered dust).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>Refund from %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Payment request %1 is too large (%2 bytes, allowed %3 bytes).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment request DoS protection</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Error communicating with %1: %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Payment request cannot be parsed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Bad response from server %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Network request error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Payment acknowledged</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PeerTableModel</name>
    <message>
        <location filename="../peertablemodel.cpp" line="+114"/>
        <source>Address/Hostname</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Ping Time</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../forms/privacydialog.ui" line="+157"/>
        <source>Zerocoin Actions:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>The displayed information may be out of date. Your wallet automatically synchronizes with the ION network after a connection is established, but this process has not completed yet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../privacydialog.cpp" line="+280"/>
        <source>Mint Zerocoin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../forms/privacydialog.ui" line="+373"/>
        <location line="+30"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+229"/>
        <source>xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-392"/>
        <source>Reset Zerocoin Wallet DB. Deletes transactions that did not make it into the blockchain.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Reset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-19"/>
        <source>Rescan the complete blockchain for  Zerocoin mints and their meta-data.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>ReScan</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Status and/or Mesages from the last Mint Action.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-360"/>
        <source>PRIVACY</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+462"/>
        <source>xION Control</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>xION Selected:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+30"/>
        <source>Quantity Selected:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <location filename="../privacydialog.cpp" line="+546"/>
        <source>Spend Zerocoin. Without &apos;Pay To:&apos; address creates payments to yourself.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../privacydialog.cpp" line="-497"/>
        <location line="+4"/>
        <location line="+12"/>
        <location line="+112"/>
        <source>Spend Zerocoin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Available (mature and spendable) xION for spending</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Available Balance:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Available (mature and spendable) xION for spending

xION are mature when they have more than 20 confirmations AND more than 2 mints of the same denomination after them were minted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-367"/>
        <location line="+49"/>
        <location line="+323"/>
        <location line="+370"/>
        <location line="+544"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <location line="+31"/>
        <source>0 xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1126"/>
        <source>Pay &amp;To:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>The ION address to send the payment to. Creates local payment to yourself when empty.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>&amp;Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Enter a label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>A&amp;mount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+63"/>
        <source>Convert Change to Zerocoin (might cost additional fees)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>If checked, the wallet tries to minimize the returning change instead of minimizing the number of spent denominations.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Minimize Change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+47"/>
        <source>Information about the available Zerocoin funds.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Zerocoin Stats:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-661"/>
        <location line="+25"/>
        <location line="+704"/>
        <location line="+38"/>
        <source>Total Balance including unconfirmed and immature xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-764"/>
        <location line="+729"/>
        <source>Total Zerocoin  Balance:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>Denominations with value 1:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 1:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <source>Unconfirmed: less than 20 confirmations
Immature: confirmed, but less than 1 mint of the same denomination after it was minted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Show the current status of automatic xION minting.

To change the status (restart required):
- enable: add 'enablezeromint=1' to ioncoin.conf
- disable: add 'enablezeromint=0' to ioncoin.conf

To change the percentage (no restart required):
- menu Settings-&gt;Options-&gt;Percentage of autominted xION

</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>AutoMint Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1178"/>
        <location line="+1227"/>
        <source>Global Supply:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Denom. 1:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 5:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 10:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 50:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 100:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 500:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 1000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Denom. 5000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-722"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <location line="+53"/>
        <source>0 x</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1092"/>
        <source>Show xION denominations list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show Denominations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>xION minting is DISABLED</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+131"/>
        <source>xION spending is NOT private (links back to the mint transaction)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+575"/>
        <source>Denominations with value 5:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 5:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 10:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 10:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 50:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 50:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 100:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 100:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 500:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 500:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 1000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 1000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Denominations with value 5000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Denom. with value 5000:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+443"/>
        <source>Hide Denominations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+138"/>
        <source>Priority:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>TextLabel</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+38"/>
        <source>Dust:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Bytes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+54"/>
        <source>Insufficient funds!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+75"/>
        <source>Coins automatically selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>medium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>Coin Control Features</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+63"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+83"/>
        <source>Amount After Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>Change:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../privacydialog.cpp" line="-413"/>
        <source>out of sync</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Mint Status: Okay</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+195"/>
        <source>Starting ResetMintZerocoin: rescanning complete blockchain, this will need up to 30 minutes depending on your hardware.
Please be patient...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+240"/>
        <source>xION Spend #: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>xION Mint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+294"/>
        <source> &lt;b&gt;enabled&lt;/b&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source> &lt;b&gt;disabled&lt;/b&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source> Configured target percentage: &lt;b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>xION is currently disabled due to maintenance.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-537"/>
        <source>xION is currently undergoing maintenance.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-215"/>
        <source>Denom. with value &lt;b&gt;1&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;5&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;10&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;50&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;100&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;500&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;1000&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. with value &lt;b&gt;5000&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+716"/>
        <source>AutoMint Status:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-713"/>
        <source>Denom. &lt;b&gt;1&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;5&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;10&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;50&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;100&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;500&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;1000&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Denom. &lt;b&gt;5000&lt;/b&gt;:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+165"/>
        <location line="+15"/>
        <location line="+241"/>
        <source>Duration: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-256"/>
        <location line="+15"/>
        <location line="+241"/>
        <source> sec.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-246"/>
        <source>Starting ResetSpentZerocoin: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+68"/>
        <source>No &apos;Pay To&apos; address provided, creating local payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid Ion Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid Send Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+22"/>
        <source>Confirm additional Fees</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+21"/>
        <source>Are you sure you want to send?&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source> to address </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source> to a newly generated (unused and therefore anonymous) local address &lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Confirm send coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Failed to fetch mint associated with serial hash</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Spend Zerocoin failed with status = </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>denomination: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-75"/>
        <source>Spending Zerocoin.
Computationally expensive, might need several minutes depending on your hardware.
Please be patient...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+76"/>
        <source>serial: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Spend is 1 of : </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>value out: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>address: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Sending successful, return code: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>txid: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>fee: </source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ProposalFrame</name>
    <message>
        <location filename="../proposalframe.cpp" line="+92"/>
        <source>Open proposal page in browser</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source> remaining payment(s).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Yes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Abstain:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>No:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+42"/>
        <source>A proposal URL can be used for phishing, scams and computer viruses. Open this link only if you trust the following URL.
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Open link</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy link</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Wallet Locked</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>You must unlock your wallet to vote.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Do you want to vote %1 on</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>using all your masternodes?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Proposal Hash:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Proposal URL:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Confirm Vote</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+64"/>
        <source>Vote Results</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../bitcoinunits.cpp" line="+252"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../guiutil.cpp" line="+105"/>
        <source>Enter a ION address (e.g. %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+773"/>
        <source>%1 d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 h</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 m</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <location line="+43"/>
        <source>%1 s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-28"/>
        <source>NETWORK</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>BLOOM</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>ZK_BLOOM</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>None</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>%1 ms</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../ion.cpp" line="+75"/>
        <location line="+7"/>
        <location line="+13"/>
        <location line="+19"/>
        <source>ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-38"/>
        <source>Error: Specified data directory &quot;%1&quot; does not exist.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Error: Cannot parse configuration file: %1. Only use key=value syntax.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Error: Invalid combination of -regtest and -testnet.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Error reading masternode configuration file: %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+40"/>
        <source>ION Core didn&apos;t yet exit safely...</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>QRImageWidget</name>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+33"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Copy Image</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+31"/>
        <source>Save QR Code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>PNG Image (*.png)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RPCConsole</name>
    <message>
        <location filename="../forms/rpcconsole.ui" line="+14"/>
        <source>Tools window</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>General</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+164"/>
        <source>Name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-157"/>
        <source>Client name</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+36"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+36"/>
        <location line="+23"/>
        <location line="+522"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+23"/>
        <location line="+26"/>
        <location line="+23"/>
        <location line="+78"/>
        <location line="+26"/>
        <location line="+29"/>
        <location line="+29"/>
        <source>N/A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1147"/>
        <source>Number of connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+134"/>
        <source>&amp;Open</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-193"/>
        <source>Startup time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+112"/>
        <source>Last block time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+42"/>
        <source>Debug log file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-258"/>
        <source>Using OpenSSL version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>Build date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+141"/>
        <source>Current number of blocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-216"/>
        <source>Client version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <source>Using BerkeleyDB version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+160"/>
        <source>Block chain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+79"/>
        <source>Open the ION debug log file from the current data directory. This can take a few seconds for large log files.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-108"/>
        <source>Number of Masternodes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+122"/>
        <source>&amp;Console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <source>Clear console</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>&amp;Network Traffic</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <source>&amp;Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Totals</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+64"/>
        <source>Received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+80"/>
        <source>Sent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+41"/>
        <source>&amp;Peers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+50"/>
        <source>Banned peers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+59"/>
        <location filename="../rpcconsole.cpp" line="+329"/>
        <location line="+727"/>
        <source>Select a peer to view detailed information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>Whitelisted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Direction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Protocol</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Version</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Services</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+92"/>
        <source>Ban Score</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Connection Time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Send</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Last Receive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Bytes Sent</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Bytes Received</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Ping Time</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>&amp;Wallet Repair</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+347"/>
        <source>Delete local Blockchain Folders</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-309"/>
        <source>Wallet In Use:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-334"/>
        <source>Starting Block</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Headers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Synced Blocks</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+184"/>
        <source>The duration of a currently outstanding ping.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Ping Wait</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Time Offset</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+101"/>
        <source>Custom Backup Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Custom xION Backup Path:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Custom Backups Threshold:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>Salvage wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Attempt to recover private keys from a corrupt wallet.dat.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Rescan blockchain files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Rescan the block chain for missing wallet transactions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Recover transactions 1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Recover transactions from blockchain (keep meta-data, e.g. account owner).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Recover transactions 2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Recover transactions from blockchain (drop meta-data).</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Upgrade wallet format</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>Rebuild block chain index from current blk000??.dat files.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>-resync:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Deletes all local blockchain folders so the wallet synchronizes from scratch.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-333"/>
        <source>The buttons below will restart the wallet with command-line options to repair the wallet, fix issues with corrupt blockhain files or missing/obsolete transactions.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-13"/>
        <source>Wallet repair options.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+286"/>
        <source>Upgrade wallet to latest format on startup. (Note: this is NOT an update of the wallet itself!)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Rebuild index</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../rpcconsole.cpp" line="-376"/>
        <source>In:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Out:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-39"/>
        <source>Welcome to the ION RPC console.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-218"/>
        <source>&amp;Disconnect Node</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <location line="+1"/>
        <source>Ban Node for</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-3"/>
        <source>1 &amp;hour</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;day</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;week</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>1 &amp;year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>&amp;Unban Node</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+95"/>
        <source>This will delete your local blockchain folders and the wallet will synchronize the complete Blockchain from scratch.&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This needs quite some time and downloads a lot of data.&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Your transactions and funds will be visible again after the download has completed.&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to continue?.&lt;br /&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Confirm resync Blockchain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+70"/>
        <source>Use up and down arrows to navigate history, and %1 to clear screen.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Type &lt;b&gt;help&lt;/b&gt; for an overview of available commands.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>WARNING: Scammers have been active, telling users to type commands here, stealing their wallet contents. Do not use this console without fully understanding the ramifications of a command.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+141"/>
        <source>%1 B</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 KB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 MB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 GB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+123"/>
        <source>(node id: %1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>via %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>never</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Inbound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Outbound</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Yes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>No</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <location line="+6"/>
        <source>Unknown</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ReceiveCoinsDialog</name>
    <message>
        <location filename="../forms/receivecoinsdialog.ui" line="+245"/>
        <source>Reuse one of the previously used receiving addresses.&lt;br&gt;Reusing addresses has security and privacy issues.&lt;br&gt;Do not use this unless re-generating a payment request made before.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>R&amp;euse an existing receiving address (not recommended)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-30"/>
        <source>&amp;Message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-87"/>
        <location line="+16"/>
        <source>An optional label to associate with the new receiving address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+25"/>
        <source>Your receiving address. You can copy and use it to receive coins on this wallet. A new one will be generated once it is used.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-22"/>
        <source>&amp;Address:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>A&amp;mount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened. Note: The message will not be sent with the payment over the ION network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-166"/>
        <source>RECEIVE</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+182"/>
        <source>An optional message to attach to the payment request, which will be displayed when the request is opened.&lt;br&gt;Note: The message will not be sent with the payment over the ION network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-107"/>
        <source>Use this form to request payments. All fields are &lt;b&gt;optional&lt;/b&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+52"/>
        <location line="+22"/>
        <source>An optional amount to request. Leave this empty or zero to not request a specific amount.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+62"/>
        <source>&amp;Request payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Clear all fields of the form.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Receiving Addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+55"/>
        <source>Requested payments history</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+62"/>
        <source>Show the selected request (does the same as double clicking an entry)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove the selected entries from the list</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Remove</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../receivecoinsdialog.cpp" line="+41"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy address</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ReceiveRequestDialog</name>
    <message>
        <location filename="../forms/receiverequestdialog.ui" line="+29"/>
        <source>QR Code</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Copy &amp;URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Copy &amp;Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Save Image...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../receiverequestdialog.cpp" line="+63"/>
        <source>Request payment to %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Payment information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>URI</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Label</source>
        <translation>Мітка</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Resulting URI too long, try to reduce the text for label / message.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Error encoding URI into QR Code.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>RecentRequestsTableModel</name>
    <message>
        <location filename="../recentrequeststablemodel.cpp" line="+27"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Label</source>
        <translation>Мітка</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-52"/>
        <source>(no label)</source>
        <translation>(без міток)</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>(no message)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>(no amount)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SendCoinsDialog</name>
    <message>
        <location filename="../forms/sendcoinsdialog.ui" line="+17"/>
        <location filename="../sendcoinsdialog.cpp" line="+234"/>
        <location line="+25"/>
        <location line="+413"/>
        <source>Send Coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+47"/>
        <source>SEND</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+147"/>
        <source>Coin Control Features</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+70"/>
        <source>Insufficient funds!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+80"/>
        <source>Quantity:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Bytes:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+48"/>
        <source>Amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Priority:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>medium</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Dust:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>no</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>After Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Change:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+62"/>
        <source>If this is activated, but the change address is empty or invalid, change will be sent to a newly generated address.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Custom change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+76"/>
        <source>Split UTXO</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source># of outputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>UTXO Size:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>0 ION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+149"/>
        <source>SwiftX technology allows for near instant transactions - A flat fee of 0.01 ION applies</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+176"/>
        <source>Transaction Fee:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Choose...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>collapse fee-settings</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Minimize</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+126"/>
        <source>per kilobyte</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>total at least</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>(read the tooltip)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-82"/>
        <source>Custom:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-202"/>
        <source>(Smart fee not initialized yet. This usually takes a few blocks...)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-101"/>
        <source>SwiftX</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Confirmation time:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-723"/>
        <source>Open Coin Control...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Coins automatically selected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1026"/>
        <source>If the custom fee is set to 1000 uIONs and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 uIONs in fee,&lt;br /&gt;while &quot;at least&quot; pays 1000 uIONs. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>If the custom fee is set to 1000 uIONs and the transaction is only 250 bytes, then &quot;per kilobyte&quot; only pays 250 uIONs in fee,&lt;br /&gt;while &quot;total at least&quot; pays 1000 uIONs. For transactions bigger than a kilobyte both pay by kilobyte.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+33"/>
        <location line="+13"/>
        <source>Paying only the minimum fee is just fine as long as there is less transaction volume than space in the blocks.&lt;br /&gt;But be aware that this can end up in a never confirming transaction once there is more demand for ION transactions than the network can process.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-365"/>
        <source>normal</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>fast</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+351"/>
        <source>Recommended</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+115"/>
        <source>Send as zero-fee transaction if possible</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>(confirmation may take longer)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+71"/>
        <source>Confirm the send action</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>S&amp;end</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+23"/>
        <source>Clear all fields of the form.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Clear &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Send to multiple recipients at once</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Add &amp;Recipient</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Anonymized ION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Balance:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../sendcoinsdialog.cpp" line="-590"/>
        <source>Copy quantity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy after fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy bytes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy priority</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy dust</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy change</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+146"/>
        <source>The split block tool does not work when sending to outside addresses. Try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+25"/>
        <source>The split block tool does not work with multiple addresses. Try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+653"/>
        <source>Warning: Invalid ION address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-615"/>
        <location line="+4"/>
        <location line="+4"/>
        <location line="+3"/>
        <source>%1 to %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+51"/>
        <source>Are you sure you want to send?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>are added as transaction fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Total Amount = &lt;b&gt;%1&lt;/b&gt;&lt;br /&gt;= %2</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+19"/>
        <source>Confirm send coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+247"/>
        <source>A fee %1 times higher than %2 per kB is considered an insanely high fee.</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+121"/>
        <source>Estimated to begin confirmation within %n block(s).</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="-152"/>
        <source>The recipient address is not valid, please recheck.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-346"/>
        <source>using SwiftX</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+38"/>
        <source> split into %1 outputs using the UTXO splitter.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+89"/>
        <source>&lt;b&gt;(%1 of %2 entries displayed)&lt;/b&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+222"/>
        <source>The amount to pay must be larger than 0.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>The amount exceeds your balance.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>The total exceeds your balance when the %1 transaction fee is included.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Duplicate address found, can only send to each address once per send operation.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Transaction creation failed!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Error: The wallet was unlocked only to anonymize coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Error: The wallet was unlocked only to anonymize coins. Unlock canceled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+86"/>
        <source>Pay only the minimum fee of %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Estimated to get 6 confirmations near instantly with &lt;b&gt;SwiftX&lt;/b&gt;!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+158"/>
        <source>Warning: Unknown change address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>(no label)</source>
        <translation>(без міток)</translation>
    </message>
</context>
<context>
    <name>SendCoinsEntry</name>
    <message>
        <location filename="../forms/sendcoinsentry.ui" line="+21"/>
        <source>This is a normal payment.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>Pay &amp;To:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>The ION address to send the payment to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+524"/>
        <location line="+536"/>
        <source>Remove this entry</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1044"/>
        <source>&amp;Label:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>Enter a label for this address to add it to the list of used addresses</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+521"/>
        <location line="+536"/>
        <source>A&amp;mount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1041"/>
        <source>Message:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>A message that was attached to the ION: URI which will be stored with the transaction for your reference. Note: This message will not be sent over the ION network.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+426"/>
        <source>This is an unverified payment request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <location line="+532"/>
        <source>Pay To:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-498"/>
        <location line="+536"/>
        <source>Memo:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-56"/>
        <source>This is a verified payment request.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../sendcoinsentry.cpp" line="+30"/>
        <source>Enter a label for this address to add it to your address book</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ShutdownWindow</name>
    <message>
        <location filename="../utilitydialog.cpp" line="+75"/>
        <source>ION Core is shutting down...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do not shut down the computer until this window disappears.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SignVerifyMessageDialog</name>
    <message>
        <location filename="../forms/signverifymessagedialog.ui" line="+14"/>
        <source>Signatures - Sign / Verify a Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+13"/>
        <source>&amp;Sign Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>You can sign messages with your addresses to prove you own them. Be careful not to sign anything vague, as phishing attacks may try to trick you into signing your identity over to them. Only sign fully-detailed statements you agree to.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+15"/>
        <source>The ION address to sign the message with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+195"/>
        <source>Choose previously used address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-185"/>
        <location line="+195"/>
        <source>Alt+A</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-188"/>
        <source>Paste address from clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Alt+P</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Enter the message you want to sign here</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Signature</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+24"/>
        <source>Copy the current signature to the system clipboard</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+18"/>
        <source>Sign the message to prove you own this ION address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+103"/>
        <source>The ION address the message was signed with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+34"/>
        <source>Verify the message to ensure it was signed with the specified ION address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-134"/>
        <source>Sign &amp;Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all sign message fields</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+137"/>
        <source>Clear &amp;All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-78"/>
        <source>&amp;Verify Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter the signing address, message (ensure you copy line breaks, spaces, tabs, etc. exactly) and signature below to verify the message. Be careful not to read more into the signature than what is in the signed message itself, to avoid being tricked by a man-in-the-middle attack.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+55"/>
        <source>Verify &amp;Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Reset all verify message fields</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../signverifymessagedialog.cpp" line="+30"/>
        <source>Click &quot;Sign Message&quot; to generate signature</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+79"/>
        <location line="+73"/>
        <source>The entered address is invalid.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-73"/>
        <location line="+7"/>
        <location line="+66"/>
        <location line="+7"/>
        <source>Please check the address and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-73"/>
        <location line="+73"/>
        <source>The entered address does not refer to a key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-66"/>
        <source>Wallet unlock was cancelled.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Private key for the entered address is not available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Message signing failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Message signed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+53"/>
        <source>The signature could not be decoded.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+12"/>
        <source>Please check the signature and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>The signature did not match the message digest.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Message verification failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Message verified.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>SplashScreen</name>
    <message>
        <location filename="../splashscreen.cpp" line="+36"/>
        <source>ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Version %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The Bitcoin Core developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The Dash Core developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The ION Core developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location filename="../networkstyle.cpp" line="+20"/>
        <source>[testnet]</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TrafficGraphWidget</name>
    <message>
        <location filename="../trafficgraphwidget.cpp" line="+79"/>
        <source>KB/s</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionDesc</name>
    <message numerus="yes">
        <location filename="../transactiondesc.cpp" line="+33"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open until %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <location line="+11"/>
        <location line="+10"/>
        <location line="+12"/>
        <source>conflicted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/offline</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/unconfirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-10"/>
        <location line="+12"/>
        <source>%1 confirmations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-37"/>
        <source>%1/offline (verified via SwiftX)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/confirmed (verified via SwiftX)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations (verified via SwiftX)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>%1/offline (SwiftX verification in progress - %2 of %3 signatures)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/confirmed (SwiftX verification in progress - %2 of %3 signatures )</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1 confirmations (SwiftX verification in progress - %2 of %3 signatures)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>%1/offline (SwiftX verification failed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>%1/confirmed (SwiftX verification failed)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+29"/>
        <source>Status</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>, has not been successfully broadcast yet</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>, broadcast through %n node(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Source</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Generated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <location line="+8"/>
        <location line="+63"/>
        <source>From</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-63"/>
        <source>unknown</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+19"/>
        <location line="+58"/>
        <source>To</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-75"/>
        <source>own address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <location line="+60"/>
        <source>watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-58"/>
        <source>label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <location line="+10"/>
        <location line="+45"/>
        <location line="+23"/>
        <location line="+50"/>
        <source>Credit</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="-126"/>
        <source>matures in %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>not accepted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+49"/>
        <location line="+22"/>
        <location line="+50"/>
        <source>Debit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-63"/>
        <source>Total debit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Total credit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Transaction fee</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Net amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <location line="+10"/>
        <source>Message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-8"/>
        <source>Comment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Output index</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+16"/>
        <source>Merchant</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Generated coins must mature %1 blocks before they can be spent. When you generated this block, it was broadcast to the network to be added to the block chain. If it fails to get into the chain, its state will change to &quot;not accepted&quot; and it won&apos;t be spendable. This may occasionally happen if another node generates a block within a few seconds of yours.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>Debug information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Transaction</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Inputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <location line="+1"/>
        <source>true</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-1"/>
        <location line="+1"/>
        <source>false</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionDescDialog</name>
    <message>
        <location filename="../forms/transactiondescdialog.ui" line="+14"/>
        <source>Transaction details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>This pane shows a detailed description of the transaction</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionTableModel</name>
    <message>
        <location filename="../transactiontablemodel.cpp" line="+215"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation type="unfinished"/>
    </message>
    <message numerus="yes">
        <location line="+57"/>
        <source>Open for %n more block(s)</source>
        <translation type="unfinished"><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform><numerusform></numerusform></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Open until %1</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Offline</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unconfirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirming (%1 of %2 recommended confirmations)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Confirmed (%1 confirmations)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Immature (%1 confirmations, will be available after %2)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>This block was not received by any other nodes and will probably not be accepted!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+38"/>
        <source>Received with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Masternode Reward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Received from</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Received via Obfuscation</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <source>ION Stake</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>xION Stake</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Obfuscation Denominate</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Obfuscation Collateral Payment</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Obfuscation Make Collateral Inputs</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Obfuscation Create Denominations</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Converted ION to xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Spent xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Received ION from xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Minted Change as xION from xION Spend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Converted xION to ION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+56"/>
        <source>Anonymous (xION Transaction)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Anonymous (xION Stake)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-86"/>
        <source>Sent to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-44"/>
        <source>Orphan Block - Generated but not accepted. This does not impact your holdings.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+46"/>
        <source>Payment to yourself</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Mined</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Obfuscated</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+44"/>
        <source>watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>(n/a)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+211"/>
        <source>Transaction status. Hover over this field to show number of confirmations.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Date and time that the transaction was received.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Type of transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Whether or not a watch-only address is involved in this transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Destination address of transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Amount removed from or added to balance.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>TransactionView</name>
    <message>
        <location filename="../transactionview.cpp" line="+69"/>
        <location line="+17"/>
        <source>All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-16"/>
        <source>Today</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This week</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This month</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Last month</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This year</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Range...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Most Common</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Received with</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sent to</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>To yourself</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Mined</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Minted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Masternode Reward</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Zerocoin Mint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Zerocoin Spend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Zerocoin Spend to Self</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Other</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enter address or label to search</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Min amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+35"/>
        <source>Copy address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy transaction ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit label</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show transaction details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Hide orphan stakes</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+228"/>
        <source>Export Transaction History</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Comma separated file (*.csv)</source>
        <translation>Файл, розділений комами (*.csv)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Confirmed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Watch-only</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Date</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Type</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Label</source>
        <translation>Мітка</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Address</source>
        <translation>Адреса</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ID</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+10"/>
        <source>Exporting Failed</source>
        <translation>Не вдалося експортувати</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>There was an error trying to save the transaction history to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-4"/>
        <source>Exporting Successful</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-310"/>
        <source>Received ION from xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Zerocoin Spend, Change in xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+307"/>
        <source>The transaction history was successfully saved to %1.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+121"/>
        <source>Range:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>to</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>UnitDisplayStatusBarControl</name>
    <message>
        <location filename="../bitcoingui.cpp" line="+120"/>
        <source>Unit to show amounts in. Click to select another unit.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletFrame</name>
    <message>
        <location filename="../walletframe.cpp" line="+26"/>
        <source>No wallet has been loaded.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletModel</name>
    <message>
        <location filename="../walletmodel.cpp" line="+334"/>
        <location line="+9"/>
        <location line="+9"/>
        <source>Send Coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-18"/>
        <location line="+9"/>
        <source>SwiftX doesn&apos;t support sending values that high yet. Transactions are currently limited to %1 ION.</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>WalletView</name>
    <message>
        <location filename="../walletview.cpp" line="+64"/>
        <source>HISTORY</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>&amp;Export</source>
        <translation>Експорт</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Export the data in the current tab to a file</source>
        <translation>Експортуйтувати дані поточної вкладки у файл</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Selected amount:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+265"/>
        <source>Backup Wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet Data (*.dat)</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>XIonControlDialog</name>
    <message>
        <location filename="../forms/xioncontroldialog.ui" line="+20"/>
        <source>Select xION to Spend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>Quantity</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+7"/>
        <location line="+14"/>
        <source>0</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-7"/>
        <source>xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+39"/>
        <source>Select/Deselect All</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+79"/>
        <source>Spendable?</source>
        <translation type="unfinished"/>
    </message>
</context>
<context>
    <name>ion-core</name>
    <message>
        <location filename="../ionstrings.cpp" line="+15"/>
        <source>(1 = keep tx meta data e.g. account owner and payment request information, 2 = drop tx meta data)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Allow JSON-RPC connections from specified source. Valid for &lt;ip&gt; are a single IP (e.g. 1.2.3.4), a network/netmask (e.g. 1.2.3.4/255.255.255.0) or a network/CIDR (e.g. 1.2.3.4/24). This option can be specified multiple times</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Bind to given address and always listen on it. Use [host]:port notation for IPv6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Bind to given address and whitelist peers connecting to it. Use [host]:port notation for IPv6</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Bind to given address to listen for JSON-RPC connections. Use [host]:port notation for IPv6. This option can be specified multiple times (default: bind to all interfaces)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Calculated accumulator checkpoint is not what is recorded by block index</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot obtain a lock on data directory %s. ION Core is probably already running.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Change automatic finalized budget voting behavior. mode=auto: Vote for only exact finalized budget match to my generated budget. (string, default: auto)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Continuously rate-limit free transactions to &lt;n&gt;*1000 bytes per minute (default:%u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Create new files with system default permissions, instead of umask 077 (only effective with disabled wallet functionality)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete all wallet transactions and only recover those parts of the blockchain through -rescan on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Delete all zerocoin spends and mints that have been recorded to the blockchain database and reindex them (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Distributed under the MIT software license, see the accompanying file COPYING or &lt;http://www.opensource.org/licenses/mit-license.php&gt;.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Enable automatic Zerocoin minting from specific addresses (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable automatic wallet backups triggered after each xION minting (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable or disable staking functionality for ION inputs (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable or disable staking functionality for xION inputs (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable spork administration functionality with the appropriate private key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter regression test mode, which uses a special chain in which blocks can be solved instantly.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Listening for incoming connections failed (listen returned error %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: The transaction is larger than the maximum allowed transaction size!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+11"/>
        <source>Error: Unsupported argument -socks found. Setting SOCKS version isn&apos;t possible anymore, only SOCKS5 proxies are supported.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Execute command when a relevant alert is received or we see a really long fork (%s in cmd is replaced by message)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Execute command when a wallet transaction changes (%s in cmd is replaced by TxID)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Execute command when the best block changes (%s in cmd is replaced by block hash)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Fees (in ION/Kb) smaller than this are considered zero fee for relaying (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Fees (in ION/Kb) smaller than this are considered zero fee for transaction creation (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Flush database activity from memory pool to disk log every &lt;n&gt; megabytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>If paytxfee is not set, include enough fee so transactions begin confirmation on average within n blocks (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>In this mode -genproclimit controls how many blocks are generated immediately.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Insufficient or insufficient confirmed funds, you might need to wait a few minutes and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Invalid amount for -maxtxfee=&lt;amount&gt;: &apos;%s&apos; (must be at least the minrelay fee of %s to prevent stuck transactions)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Keep the specified amount available for spending at all times (default: 0)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Log transaction priority and fee per kB when mining blocks (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Maintain a full transaction index, used by the getrawtransaction rpc call (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Maximum average size of an index occurrence in the block spam filter (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Maximum size of data in data carrier transactions we relay and mine (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Maximum size of the list of indexes in the block spam filter (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Maximum total fees to use in a single wallet transaction, setting too low may abort large transactions (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Number of seconds to keep misbehaving peers from reconnecting (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Obfuscation uses exact denominated amounts to send funds, you might simply need to anonymize some more coins.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Output debugging information (default: %u, supplying &lt;category&gt; is optional)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Query for peer addresses via DNS lookup, if low on addresses (default: 1 unless -connect)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Randomize credentials for every proxy connection. This enables Tor stream isolation (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Require high priority for relaying free or low-fee transactions (default:%u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Send trace/debug info to console instead of debug.log file (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Set maximum size of high-priority/low-fee transactions in bytes (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Set the number of included blocks to precompute per cycle. (minimum: %d) (maximum: %d) (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Set the number of script verification threads (%u to %d, 0 = auto, &lt;0 = leave that many cores free, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Set the number of threads for coin generation if enabled (-1 = all cores, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Show N confirmations for a successfully locked transaction (0-9999, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+12"/>
        <source>Support filtering of blocks and transaction with bloom filters (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>The block database contains a block which appears to be from the future. This may be due to your computer&apos;s date and time being set incorrectly. Only rebuild the block database if you are sure that your computer&apos;s date and time are correct</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit &lt;https://www.openssl.org/&gt; and cryptographic software written by Eric Young and UPnP software written by Thomas Bernard.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Total length of network version string (%i) exceeds maximum length (%i). Reduce the number or size of uacomments.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to bind to %s on this computer. ION Core is probably already running.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to locate enough Obfuscation denominated funds for this transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Unable to locate enough Obfuscation non-denominated funds for this transaction that are not equal 20000 ION.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to locate enough funds for this transaction that are not equal 20000 ION.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Use separate SOCKS5 proxy to reach peers via Tor hidden services (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: -maxtxfee is set very high! Fees this large could be paid on a single transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: -paytxfee is set very high! This is the transaction fee you will pay if you send a transaction.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: Please check that your computer&apos;s date and time are correct! If your clock is wrong ION Core will not work properly.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: The network does not appear to fully agree! Some miners appear to be experiencing issues.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: We do not appear to fully agree with our peers! You may need to upgrade, or other nodes may need to upgrade.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: error reading wallet.dat! All keys read correctly, but transaction data or address book entries might be missing or incorrect.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Warning: wallet.dat corrupt, data salvaged! Original wallet.dat saved as wallet.{timestamp}.bak in %s; if your balance or transactions are incorrect you should restore from a backup.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitelist peers connecting from the given netmask or IP address. Can be specified multiple times.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Whitelisted peers cannot be DoS banned and their transactions are always relayed, even if they are already in the mempool, useful e.g. for a gateway</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>You must specify a masternodeprivkey in the configuration. Please see documentation for help.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>(12700 could be used only on mainnet)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>(default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>(default: 1)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>(must be 12700 for mainnet)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Accept command line and JSON-RPC commands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Accept connections from outside (default: 1 if no -proxy or -connect)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Accept public REST requests (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Add a node to connect to and attempt to keep the connection open</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Adding Wrapped Serials supply...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Allow DNS lookups for -addnode, -seednode and -connect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Always query for peer addresses via DNS lookup (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Append comment to the user agent string</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Attempt to recover private keys from a corrupt wallet.dat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Automatically create Tor hidden service (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Block creation options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Calculating missing accumulators...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot downgrade wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -bind address: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -externalip address: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot resolve -whitebind address: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot write default address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>CoinSpend: failed check</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connect only to the specified node(s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connect through SOCKS5 proxy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connect to a node to retrieve peer addresses, and disconnect</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Connection options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2009-%i The Bitcoin Core Developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2014-%i The Dash Core Developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2015-%i The PIVX Core Developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Copyright (C) 2018-%i The ION Core Developers</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Corrupted block database detected</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not parse masternode.conf</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Couldn&apos;t generate the accumulator witness</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Debugging/Testing options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete blockchain folders and resync from scratch</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Disable OS notifications for incoming transactions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Disable safemode, override a real safe mode event (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Discover own IP address (default: 1 when listening and no -externalip)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Do not load the wallet and disable wallet RPC calls</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to rebuild the block database now?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Done loading</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable automatic Zerocoin minting (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable precomputation of xION spends and stakes (0-1, default %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable publish hash transaction (locked via SwiftX) in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable publish raw transaction (locked via SwiftX) in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Enable the client to act as a masternode (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error initializing wallet database environment %s!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading wallet.dat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading wallet.dat: Wallet corrupted</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error loading wallet.dat: Wallet requires newer version of ION Core</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error opening block database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error reading from database, shutting down.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error recovering public key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error writing zerocoinDB to disk</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: A fatal internal error occured, see debug.log for details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Disk space is low!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: Unsupported argument -tor found, use -onion.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Error: Wallet locked, unable to create transaction!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to calculate accumulator checkpoint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to listen on any port. Use -listen=0 if you want this.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to parse host:port string</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to read block</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Fee (in ION/kB) to add to transactions you send (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Force safe mode (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Generate coins (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>How many blocks to check at startup (default: %u, 0 = all)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>If &lt;category&gt; is not supplied, output all debugging information.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Importing...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Imports blocks from external blk000??.dat file</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Include IP addresses in debug output (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Incorrect or no genesis block found. Wrong datadir for network?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Information</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Initialization sanity check failed. ION Core is shutting down.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Insufficient funds.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid -onion address or hostname: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -maxtxfee=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -minrelaytxfee=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -mintxfee=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -paytxfee=&lt;amount&gt;: &apos;%s&apos; (must be at least %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -paytxfee=&lt;amount&gt;: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount for -reservebalance=&lt;amount&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid amount</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid masternodeprivkey. Please see documenation.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid netmask specified in -whitelist: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid port detected in masternode.conf</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid private key.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+32"/>
        <source>Percentage of automatically minted Zerocoin  (1-100, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Recalculating ION supply...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Recalculating minted XION...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Recalculating spent XION...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Reindex the ION and xION money supply statistics</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Reindexing zerocoin database...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Reindexing zerocoin failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Selected coins value is less than payment target</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+28"/>
        <source>Support the zerocoin light node protocol (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>SwiftX options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-247"/>
        <source>This is a pre-release test build - use at your own risk - do not use for staking or merchant applications!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-187"/>
        <source> mints deleted
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source> mints updated, </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source> unconfirmed transactions removed
</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+38"/>
        <source>Disable all ION specific functionality (Masternodes, Zerocoin, SwiftX, Budgeting) (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enable SwiftX, show confirmations for locked transactions (bool, default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>Error: The transaction was rejected! This might happen if some of the coins in your wallet were already spent, such as if you used a copy of wallet.dat and coins were spent in the copy but not marked as spent here.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Error: This transaction requires a transaction fee of at least %s because of its amount, complexity, or use of recently received funds!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Error: Unsupported argument -checklevel found. Checklevel must be level 4.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Execute command when the best block changes and its size is over (%s in cmd is replaced by block hash, %d with the block size)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to find coin set amongst held coins with less than maxNumber of Spends</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>In rare cases, a spend with 7 coins exceeds our maximum allowable transaction size, please retry spend using 6 or less coins</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+37"/>
        <source>Preferred Denomination for automatically minted Zerocoin  (1/5/10/50/100/500/1000/5000), 0 for no preference. default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+27"/>
        <source>Specify custom backup path to add a copy of any automatic xION backup. If set as dir, every backup generates a timestamped file. If set as file, will rewrite to that file every backup. If backuppath is set as well, 4 backups will happen</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Specify custom backup path to add a copy of any wallet backup. If set as dir, every backup generates a timestamped file. If set as file, will rewrite to that file every backup.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>SwiftX requires inputs with at least 6 confirmations, you might need to wait a few minutes and try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+66"/>
        <source>&lt;category&gt; can be:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+9"/>
        <source>Attempt to force blockchain corruption recovery</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot create public spend input</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>CoinSpend: Accumulator witness does not verify</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+17"/>
        <source>Display the stake modifier calculations in the debug.log file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Display verbose coin stake messages in the debug.log file.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+6"/>
        <source>Enable publish hash block in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable publish hash transaction in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable publish raw block in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Enable publish raw transaction in &lt;address&gt;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable staking functionality (0-1, default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+14"/>
        <source>Error: A fatal internal error occurred, see debug.log for details</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Error: No valid utxo!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Failed to create mint</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to find Zerocoins in wallet.dat</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to parse public spend</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Failed to select a zerocoin</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to wipe zerocoinDB</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Failed to write coin serial number into wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+26"/>
        <source>Keep at most &lt;n&gt; unconnectable transactions in memory (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Limit size of signature cache to &lt;n&gt; entries (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Line: %d</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Listen for JSON-RPC connections on &lt;port&gt; (default: %u or testnet: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Listen for connections on &lt;port&gt; (default: %u or testnet: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading addresses...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading block index...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading budget cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading masternode cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading masternode payment cache...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading sporks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet... (%3.2f %%)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Loading wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Location of the auth cookie (default: data dir)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Lock masternodes from masternode configuration file (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Lookup(): Invalid -proxy address or hostname: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Maintain at most &lt;n&gt; connections to peers (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Masternode options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximum per-connection receive buffer, &lt;n&gt;*1000 bytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Maximum per-connection send buffer, &lt;n&gt;*1000 bytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Mint did not make it into blockchain</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Need address because change is not exact</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Need to specify a port with -whitebind: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Node relay options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Not enough file descriptors available.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Number of automatic wallet backups (default: 10)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Number of custom location backups to retain (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Only accept block chain matching built-in checkpoints (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Only connect to nodes in network &lt;net&gt; (ipv4, ipv6 or onion)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Password for JSON-RPC connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+81"/>
        <source>Unable to find transaction containing mint %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to find transaction containing mint, txHash: %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+8"/>
        <source>Use block spam filter (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+20"/>
        <source>could not get lock on cs_spendcache</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>isValid(): Invalid -proxy address or hostname: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="-109"/>
        <source>Preparing for resync...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Prepend debug output with timestamp (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Print version and exit</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Pubcoin not found in mint tx</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>RPC server options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Randomly drop 1 of every &lt;n&gt; network messages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Randomly fuzz 1 of every &lt;n&gt; network messages</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Rebuild block chain index from current blk000??.dat files</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+4"/>
        <source>Receive and display P2P network alerts (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Reindex the accumulator database</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Relay and mine data carrier transactions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Relay non-P2SH multisig (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescan the block chain for missing wallet transactions</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Rescanning...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>ResetMintZerocoin finished: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>ResetSpentZerocoin finished: </source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Run a thread to flush wallet periodically (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Run in the background as a daemon and accept commands</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Send transactions as zero-fee transactions if possible (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Session timed out.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set database cache size in megabytes (%d to %d, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set external address:port to get to this masternode (example: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set key pool size to &lt;n&gt; (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set maximum block size in bytes (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set minimum block size in bytes (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set the Maximum reorg depth (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set the masternode private key</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Set the number of threads to service RPC calls (default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Sets the DB_PRIVATE flag in the wallet db environment (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Show all debugging options (usage: --help -help-debug)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Shrink debug.log file on client startup (default: 1 when no -debug)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing failed.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing timed out.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Signing transaction failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify configuration file (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify connection timeout in milliseconds (minimum: 1, default: %d)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify data directory</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify masternode configuration file (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify pid file (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify wallet file (within data directory)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Specify your own public address</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Spend Valid</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Spend unconfirmed change when sending transactions (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Staking options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Stop running after importing blocks from disk (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Synchronization failed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization finished</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization pending...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronizing budgets...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronizing masternode winners...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronizing masternodes...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronizing sporks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Syncing xION wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The coin spend has been used</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>The transaction did not verify</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This help message</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This is experimental software.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>This is intended for regression testing tools and app development.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Threshold for disconnecting misbehaving peers (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Too many spends needed</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Tor control port password (default: empty)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Tor control port to use if onion listening enabled (default: %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Created</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction Mint Started</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amount too small</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction amounts must be positive</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large for fee policy</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Transaction too large</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Trying to spend an already spent serial #, try again.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to bind to %s on this computer (bind returned error %s)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>Unable to sign spork message, wrong key?</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to start HTTP server. See debug log for details.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown network specified in -onlynet: &apos;%s&apos;</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Upgrade wallet to latest format</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Use UPnP to map the listening port (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Use UPnP to map the listening port (default: 1 when listening)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Use a custom max chain reorganization depth (default: %u)</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+2"/>
        <source>Use the test network</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>User Agent comment (%s) contains unsafe characters.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Username for JSON-RPC connections</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Value is below the smallest available denomination (= 1) of xION</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying blocks...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Verifying wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet %s resides outside data directory %s</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet needed to be rewritten: restart ION Core to complete</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Wallet window title</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: This version is obsolete, upgrade required!</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: Unsupported argument -benchmark ignored, use -debug=bench.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Warning: Unsupported argument -debugnet ignored, use -debug=net.</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>You don&apos;t have enough Zerocoins in your wallet</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>You need to rebuild the database using -reindex to change -txindex</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Zapping all transactions from wallet...</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>ZeroMQ notification options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>Zerocoin options:</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+3"/>
        <source>on startup</source>
        <translation type="unfinished"/>
    </message>
    <message>
        <location line="+1"/>
        <source>wallet.dat corrupt, salvage failed</source>
        <translation type="unfinished"/>
    </message>
</context>
</TS>